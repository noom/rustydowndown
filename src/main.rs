use quicksilver::{
    geom::Vector,
    lifecycle::{run, Settings},
};

mod bonus;
mod gamestate;
mod obstacles;
mod player;
mod text;
mod timer;

use crate::gamestate::GameState;

pub const FPS: f64 = 60.;
pub const UPDATE_RATE: f64 = 1000. / FPS;

pub const WINDOW_WH: (f32, f32) = (500., 600.);

pub const TEXT_DURATION: f64 = 3.;

pub const HARDER_STEP_DURATION: f64 = 25.;

pub const BONUS_CHANCE: i32 = 10;
pub const BONUS_DURATION: f64 = 7.;

pub const PLAYER_SPEED: f32 = 260.;
pub const OBSTACLES_SPEED: f32 = 160.;

pub const OBSTACLES_SLOWLINESS: f32 = 70.;

pub const DIFFICULTY_STEP: f32 = 0.10;

fn main() {
    let mut settings = Settings::default();
    settings.vsync = true;
    settings.update_rate = UPDATE_RATE;

    // run::<GameState>("GameState", Vector::new(WINDOW_WH.0, WINDOW_WH.1), settings);
    run::<GameState>("GameState", Vector::new(WINDOW_WH.0, WINDOW_WH.1), Settings::default());
}
