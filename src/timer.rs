use crate::UPDATE_RATE;

pub struct Timer {
    progress: f64,
    max_secs: f64,
}

impl Timer {
    pub fn new(max_secs: f64) -> Self {
        let progress = 0.;
        let max_secs = max_secs * 1000.;

        Self { progress, max_secs }
    }

    pub fn step(&mut self) -> bool {
        self.progress += UPDATE_RATE;

        if self.progress >= self.max_secs {
            true
        } else {
            false
        }
    }

    pub fn reset(&mut self) {
        self.progress = 0.;
    }

    pub fn _is_running(&mut self) -> bool {
        self.progress < self.max_secs
    }
}
