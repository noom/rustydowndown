use quicksilver::{
    geom::Rectangle,
    graphics::{Background::Col, Color},
    input::Key,
    lifecycle::Window,
};

use crate::{DIFFICULTY_STEP, FPS, PLAYER_SPEED, WINDOW_WH};

pub struct Player {
    pub shape: Rectangle,

    speed: f32,

    falling_speed: f32,
    pub falling: bool,

    difficulty: f32,
}

impl Player {
    pub fn new() -> Self {
        let shape = Rectangle::new((WINDOW_WH.0 as i32 / 2 - 30 / 2, 30), (30, 30));

        let speed = PLAYER_SPEED / FPS as f32;

        let falling_speed = 160. / FPS as f32;
        let falling = true;

        let difficulty = 1.;

        Self {
            shape,
            speed,
            falling_speed,
            falling,
            difficulty,
        }
    }

    pub fn update(&mut self, window: &mut Window) {
        if window.keyboard()[Key::A].is_down() {
            self.shape.pos.x -= self.speed;
        }
        if window.keyboard()[Key::D].is_down() {
            self.shape.pos.x += self.speed;
        }

        if self.falling {
            self.shape.pos.y += self.falling_speed;
        }
    }

    pub fn draw(&mut self, window: &mut Window) {
        window.draw(&self.shape, Col(Color::RED));
    }

    pub fn harder(&mut self) {
        self.difficulty += DIFFICULTY_STEP;
        self.reset_speed();
    }

    pub fn reset_speed(&mut self) {
        self.speed = PLAYER_SPEED / FPS as f32 * self.difficulty
    }

    pub fn set_speed(&mut self, speed: f32) {
        self.speed = speed / FPS as f32;
    }
}
