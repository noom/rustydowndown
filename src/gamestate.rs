use quicksilver::{
    combinators::result,
    geom::Shape,
    graphics::{Background::Img, Color, Font, FontStyle, Image},
    lifecycle::{Asset, State, Window},
    Future, Result,
};

use std::collections::HashMap;

use crate::{
    bonus::Effect,
    obstacles::Obstacles,
    player::Player,
    text::{Text, TextRenderer},
    timer::Timer,
    {BONUS_DURATION, FPS, HARDER_STEP_DURATION, PLAYER_SPEED, TEXT_DURATION, WINDOW_WH},
};

pub struct GameState {
    player: Player,
    obstacles: Obstacles,

    score: f32,

    bonus_effect: Option<Effect>,
    bonus_timer: Option<Timer>,

    harder_timer: Timer,

    text_current: Option<Text>,
    text_timer: Timer,
    text_renderer: TextRenderer,
}

impl State for GameState {
    fn new() -> Result<Self> {
        let player = Player::new();
        let obstacles = Obstacles::new(5);

        let score = 0.;

        let bonus_effect = None;
        let bonus_timer = None;

        let harder_timer = Timer::new(HARDER_STEP_DURATION);

        let text_current = None;
        let text_timer = Timer::new(TEXT_DURATION);

        let text_renderer = TextRenderer::new();

        Ok(Self {
            player,
            obstacles,
            score,
            bonus_effect,
            bonus_timer,
            harder_timer,
            text_current,
            text_timer,
            text_renderer,
        })
    }

    fn update(&mut self, window: &mut Window) -> Result<()> {
        // println!("FPS: {}", window.current_fps());

        self.player.update(window);
        self.obstacles.update(window);

        if let Some((pos_y, bonus_effect)) = self.obstacles.collide(&self.player.shape) {
            self.player.shape.pos.y = pos_y;
            self.player.falling = false;

            if self.bonus_effect.is_none() {
                if let Some(effect) = bonus_effect {
                    self.apply_bonus(effect);
                }
            }
        } else if self.player.shape.pos.y >= WINDOW_WH.1 - self.player.shape.size.y {
            self.player.falling = false;
        } else {
            self.player.falling = true;
        }

        if self.player.shape.pos.x < 0. {
            self.player.shape.pos.x = WINDOW_WH.0 - self.player.shape.size.x;
        } else if self.player.shape.pos.x + self.player.shape.size.x > WINDOW_WH.0 {
            self.player.shape.pos.x = 0.;
        }

        if self.bonus_timer.is_some() && self.bonus_timer.as_mut().unwrap().step() {
            self.remove_bonus();
        }

        if self.text_timer.step() {
            self.text_current = None;
        }

        if self.harder_timer.step() {
            self.harder_timer.reset();
            self.obstacles.harder();
            self.player.harder();
            self.show_text(Text::Harder);
        }

        self.score += 1. / FPS as f32;

        Ok(())
    }

    fn draw(&mut self, window: &mut Window) -> Result<()> {
        window.clear(Color::BLACK)?;

        self.player.draw(window);
        self.obstacles.draw(window);

        if let Some(text_current) = &self.text_current {
            self.text_renderer
                .draw(window, (WINDOW_WH.0 / 2., 20.), &text_current)?;
        }

        self.text_renderer.draw(
            window,
            (WINDOW_WH.0 - 75., 20.),
            &Text::Number(self.score as i32),
        )?;

        // let style = FontStyle::new(32.0, Color::RED);
        // let image = self
        //     .font
        //     .render(format!("Score: {}", self.score as i32).as_str(), &style)
        //     .unwrap();

        //         window.draw(
        //             &image.area().with_center((WINDOW_WH.0 - 150., 10)),
        //             Img(&image),
        //         );

        Ok(())
    }
}

impl GameState {
    fn apply_bonus(&mut self, effect: Effect) {
        match effect {
            Effect::FasterPlayer => {
                self.player.set_speed(PLAYER_SPEED + 15.);
                self.show_text(Text::BonusFasterPlayer);
            }
            Effect::SlowerObstacles => {
                self.obstacles.lower_speed();
                self.show_text(Text::BonusSlowerObstacles);
            }
        }

        self.bonus_effect = Some(effect);
        self.bonus_timer = Some(Timer::new(BONUS_DURATION));
    }

    fn remove_bonus(&mut self) {
        match self.bonus_effect.as_ref().unwrap() {
            Effect::FasterPlayer => self.player.set_speed(PLAYER_SPEED),
            Effect::SlowerObstacles => self.obstacles.reset_speed(),
        }

        self.bonus_effect = None;
        self.bonus_timer = None;
    }

    fn show_text(&mut self, text: Text) {
        self.text_current = Some(text);
        self.text_timer.reset();
    }
}
