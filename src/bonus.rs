use quicksilver::graphics::Color;

use rand::{
    distributions::{Distribution, Standard},
    prelude::*,
};

pub type Bonus = (Effect, Side, Color);

#[derive(Clone, Debug)]
pub enum Side {
    Left,
    Right,
}

impl Distribution<Side> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Side {
        match rng.gen_range(0, 2) {
            0 => Side::Left,
            1 => Side::Right,
            _ => self.sample(rng),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Effect {
    FasterPlayer,
    SlowerObstacles,
}

impl Distribution<Effect> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Effect {
        match rng.gen_range(0, 2) {
            0 => Effect::FasterPlayer,
            1 => Effect::SlowerObstacles,
            _ => self.sample(rng),
        }
    }
}
