use quicksilver::{
    geom::{Rectangle, Shape},
    graphics::{Background::Col, Color},
    lifecycle::Window,
};

use rand::prelude::*;

use hsl::HSL;

use crate::{bonus::{Bonus, Effect, Side},
BONUS_CHANCE, DIFFICULTY_STEP, FPS, OBSTACLES_SLOWLINESS, OBSTACLES_SPEED, WINDOW_WH};

struct Obstacle {
    pub shape: Rectangle,
    speed: f32,
    bonus: Option<Bonus>,
}

impl Obstacle {
    fn new(pos_y: f32, speed: f32, bonus_available: bool) -> Self {
        let mut rng = thread_rng();

        let length = rng.gen_range(75, 151);
        let pos_x = rng.gen_range(0, WINDOW_WH.0 as i32 - length);

        let shape = Rectangle::new((pos_x, pos_y), (length, 4));
        let speed = speed / FPS as f32;

        let bonus: Option<Bonus> = if bonus_available {
            Some((rand::random(), rand::random(), Color::WHITE))
        } else {
            None
        };

        Obstacle {
            shape,
            speed,
            bonus,
        }
    }

    fn update(&mut self) {
        self.shape.pos.y -= self.speed;

        if let Some(bonus) = &mut self.bonus {
            let new_color_rgb = HSL {
                h: thread_rng().gen_range(0, 361) as f64,
                s: 1.,
                l: thread_rng().gen_range(40, 60) as f64 / 100.,
            }
            .to_rgb();

            let new_color = Color {
                r: new_color_rgb.0 as f32 / 255.,
                g: new_color_rgb.1 as f32 / 255.,
                b: new_color_rgb.2 as f32 / 255.,
                a: 1.,
            };

            bonus.2 = new_color;
        }
    }

    fn draw(&self, window: &mut Window) {
        let first_rect =
            Rectangle::new((0, self.shape.pos.y), (self.shape.pos.x, self.shape.size.y));

        let second_rect = Rectangle::new(
            (self.shape.pos.x + self.shape.size.x, self.shape.pos.y),
            (
                WINDOW_WH.0 - (self.shape.pos.x + self.shape.size.x),
                self.shape.size.y,
            ),
        );

        let mut first_color = Color::WHITE;
        let mut second_color = Color::WHITE;

        if let Some(bonus) = &self.bonus {
            match bonus.1 {
                Side::Left => first_color = bonus.2,
                Side::Right => second_color = bonus.2,
            }
        }

        window.draw(&first_rect, Col(first_color));
        window.draw(&second_rect, Col(second_color));
    }

    fn set_speed(&mut self, speed: f32) {
        self.speed = speed / FPS as f32;
    }
}

pub struct Obstacles {
    max: usize,
    obstacles: Vec<Obstacle>,

    difficulty: f32,
    slowliness: f32,
}

impl Obstacles {
    pub fn new(max: usize) -> Self {
        let obstacles = Vec::new();

        let difficulty = 1.;
        let slowliness = 0.;

        Obstacles {
            max,
            obstacles,
            difficulty,
            slowliness,
        }
    }

    pub fn update(&mut self, _window: &mut Window) {
        for obstacle in &mut self.obstacles {
            obstacle.update();
        }

        if !self.obstacles.is_empty() && self.obstacles[0].shape.pos.y <= 0. {
            self.obstacles.remove(0);
        }

        let spacing = WINDOW_WH.1 / self.max as f32;

        if self.obstacles.is_empty()
            || (self.obstacles.len() < self.max
                && self.obstacles.last().unwrap().shape.pos.y <= WINDOW_WH.1 - spacing)
        {
            let bonus_available = thread_rng().gen_range(1, BONUS_CHANCE + 1) == BONUS_CHANCE;
            self.obstacles.push(Obstacle::new(
                WINDOW_WH.1,
                OBSTACLES_SPEED * self.difficulty - self.slowliness,
                bonus_available,
            ));
        }
    }

    pub fn draw(&mut self, window: &mut Window) {
        for line in &self.obstacles {
            line.draw(window);
        }
    }

    pub fn collide(&self, shape: &Rectangle) -> Option<(f32, Option<Effect>)> {
        for obstacle in &self.obstacles {
            let hole = obstacle.shape;
            let left = Rectangle::new((0, hole.pos.y), (hole.pos.x, 4));
            let right = Rectangle::new(
                (hole.pos.x + hole.size.x, hole.pos.y),
                (WINDOW_WH.0 - hole.pos.x + hole.size.x, 4),
            );

            if shape.overlaps(&left) || shape.overlaps(&right) {
                let position = hole.pos.y - shape.size.y;

                let effect = if let Some((effect, side, _)) = &obstacle.bonus {
                    match side {
                        Side::Left => {
                            if shape.overlaps(&left) {
                                Some(effect.clone())
                            } else {
                                None
                            }
                        }
                        Side::Right => {
                            if shape.overlaps(&right) {
                                Some(effect.clone())
                            } else {
                                None
                            }
                        }
                    }
                } else {
                    None
                };

                return Some((position, effect));
            }
        }

        None
    }

    pub fn harder(&mut self) {
        self.difficulty += DIFFICULTY_STEP;
        self.reset_speed();
    }

    pub fn lower_speed(&mut self) {
        self.slowliness = OBSTACLES_SLOWLINESS;

        for obstacle in &mut self.obstacles {
            obstacle.set_speed(OBSTACLES_SPEED * self.difficulty - self.slowliness);
        }
    }

    pub fn reset_speed(&mut self) {
        self.slowliness = 0.;

        for obstacle in &mut self.obstacles {
            obstacle.set_speed(OBSTACLES_SPEED * self.difficulty);
        }
    }
}
